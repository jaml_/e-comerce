import { Component, OnInit } from '@angular/core';
import { Product } from './product.model';

import { PRODUCTS } from 'src/app/mocks/products.mock';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  products: Product[] = PRODUCTS;
  constructor() { }
  ngOnInit() {
  }

  trackByFn(index, item) {
    if (!item) {
      return null;
    }
    return index;
  }
}
