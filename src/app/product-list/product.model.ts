export interface Product {
    id: string;
    title: string;
    image: string;
    quantity: number;
    price: number;
    description: string;
}