import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { PRODUCTS } from 'src/app/mocks/products.mock';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {
  product;
  constructor(
      private route: ActivatedRoute,
      private cartService: CartService
    ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.product = PRODUCTS[+params.get('productId')];
    });
  }

  plus() {
    this.product.quantity++;
  }

  dismiss() {
    if (this.product.quantity > 1) {
      this.product.quantity--;
    }
  }

  addToCart(product) {
    this.cartService.addToCart(product);
    window.alert('Product added.');
  }

}
